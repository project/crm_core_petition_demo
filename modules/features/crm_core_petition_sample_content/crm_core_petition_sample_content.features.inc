<?php
/**
 * @file
 * crm_core_petition_sample_content.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function crm_core_petition_sample_content_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "crm_core_profile" && $api == "crm_core_profile") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
