<?php
/**
 * @file
 * Code for the CRM Core Petition sample content feature.
 */

include_once 'crm_core_petition_sample_content.features.inc';

/**
 * Implements hook_uuid_node_features_export_render_alter().
 *
 * Adding path alias and menu link to export.
 */
function crm_core_petition_sample_content_uuid_node_features_export_render_alter(&$export, $node, $module) {

  $nids = entity_get_id_by_uuid('node', array($node->uuid));
  $path = 'node/' . $nids[$node->uuid];
  $alias = drupal_get_path_alias($path);
  if ($path !== $alias) {
    $export->url_alias = $alias;
  }

  // Export the menu, if it's attached to the node.
  if (module_exists('menu')) {
    $uuid = $node->uuid;
    $nids = entity_get_id_by_uuid('node', array($uuid));
    $node->nid = $nids[$uuid];
    menu_node_prepare($node);
    if (isset($node->menu)) {
      if (isset($node->menu['link_title']) && !empty($node->menu['link_title'])) {
        $export->new_menu = $node->menu;
      }
    }
    unset($node->nid);
    unset($node->menu);
  }
}

/**
 * Implements hook_node_insert().
 *
 * Stores menu link.
 */
function crm_core_petition_sample_content_node_insert($node) {

  if (isset($node->url_alias)) {
    // Check to see if the alias already exists.
    $check = drupal_get_normal_path($node->url_alias);
    // If so, delete the alias so we can create it again.
    if ($check === $node->url_alias) {
      // Delete the alias.
      path_delete(array('alias' => $check));
    }
    // Save the alias.
    $path = array(
      'alias' => $node->url_alias,
      'source' => 'node/' . $node->nid,
    );
    // Save.
    path_save($path);
    unset($node->url_alias);
  }

  // When content is imported, let's make sure we save whatever menu stuff needs
  // saving there should be an item with an exported menu item.
  if (module_exists('menu') && isset($node->new_menu)) {
    // Add the menu item.
    $item = array(
      'link_path' => 'node/' . $node->nid,
      'link_title' => $node->new_menu['link_title'],
      'menu_name' => $node->new_menu['menu_name'],
      'hidden' => $node->new_menu['hidden'],
      'weight' => $node->new_menu['weight'],
    );
    menu_link_save($item);
    // But you can't do this every time... only when you are importing content.
    unset($node->new_menu);
  }
}

/**
 * Helper to rebuild uuid_node feature component.
 */
function _crm_core_petition_sample_content_rebuild_nodes() {
  // Saving profile nodes configuration.
  $module = 'crm_core_petition_sample_content';
  // Including sample nodes definitions.
  module_load_include('inc', $module, $module . '.features.uuid_node');
  // Including helper.
  module_load_include('inc', 'uuid_features', 'includes/uuid_node.features');
  // Creating sample content in DB.
  uuid_node_features_rebuild($module);
  crm_core_petition_sample_content_post_features_rebuild('uuid_node');
}

/**
 * Implements hook_post_features_rebuild().
 */
function crm_core_petition_sample_content_post_features_rebuild($component) {
  // Configure the default email settings, if they are enabled.
  if (isset($component) && $component == 'uuid_node' && variable_get('configure_email', 0) === 1) {
    $fields = array();
    $fields['sign_email'] = 'rules_cmcp_target_email';
    $fields['thanks_email'] = 'rules_cmcp_thank_you_message';
    db_update('crm_core_petition_nodes')->fields($fields)->execute();
  }
}

/**
 * Implements hook_post_features_rebuild().
 */
function crm_core_petition_sample_content_post_features_revert($component) {
  crm_core_petition_sample_content_post_features_rebuild($component);
}
